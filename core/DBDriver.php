<?php

namespace core;

class DBDriver
{
    const FETCH_ALL = 'all'; // константа на переданный параметр
    const FETCH_ONE = 'one';
    private $pdo;
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }
    // метод выбора экшена
    public function select($sql, array $params = [], $fetch = self::FETCH_ALL)
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        if($fetch !== self::FETCH_ALL && $fetch !== self::FETCH_ONE){
            echo "Неправильный параметр передан";
        }

        return $fetch === self::FETCH_ALL ? $stmt->fetchAll() : $stmt->fetch();
    }

    public function del($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
    }

    public function insert($table, array $params)
    {
        $columns = sprintf('%s', implode(', ', array_keys($params)));
        $masks = sprintf(':%s', implode(', :', array_keys($params)));

        $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', $table, $columns, $masks);
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

//        if($table === 'users'){
//            header('Location: / ');
//            die;
//        }
//        return $id = $this->pdo->lastInsertId();
//        $way = substr($table,0,-1); // удаляем последний символ из переменной -> получаем контроллер
//        header(sprintf('Location: /%s/%s', $way, $id));
//        die;
    }

    public function edits($table, array $params)
    {
        $sql = "UPDATE $table SET title = :title, text = :text WHERE id = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            'title' => $params['title'],
            'text' => $params['text'],
            'id' => $params['id']
        ]);
    }

}