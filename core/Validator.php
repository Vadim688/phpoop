<?php

namespace core;

class Validator
{
    public $errors = [];
    public $clean = [];
    public $success = false;
    private $rules;

    public function setRules(array $rules)
    {

        $this->rules = $rules;
    }

    public function execute(Array $fields)
    {
        if(!$this->rules){
            die('notRules');
        }

        foreach($this->rules as $name => $rules){

            if(isset($rules['require']) && !isset($fields[$name])){
                $this->errors[$name] = sprintf('Field %s is require', $name);
            }

            if(isset($rules['require']) && $fields[$name] === ''){
                $this->errors[$name] = sprintf('Field %s is empty', $name);

            }

            if(isset($rules['min_length']) && mb_strlen($fields[$name],'utf-8') < $rules['min_length']){
                $this->errors[$name] = sprintf('Field %s less %s', $name, $rules['min_length']);

            }

            if(isset($rules['max_length']) && mb_strlen($fields[$name],'utf-8') > $rules['max_length']){
                $this->errors[$name] = sprintf('Field %s more %s', $name, $rules['max_length']);
            }

            if(isset($rules['type'])){
                if($rules['type'] === 'string'){
                    $fields[$name] = trim(htmlspecialchars($fields[$name]));
                }elseif ($rules['type'] === 'integer'){
                    if(!is_numeric($fields['name'])){
                        $this->errors[$name] = sprintf('');
                    }
                }
            }

//            var_dump($this->clean);
//            die();
            ////////////////////////////////////////////////////////
            if($this->errors[$name] === null && isset($fields[$name])){ // записываем в поле clean что проверка прошла
                $this->success = true;
                $this->clean[$name] = htmlspecialchars(trim($fields[$name]));


            } elseif(isset($rules['type']) && $rules['type'] === 'integer'){
                $this->clean[$name] = (int)$fields[$name];

            }
            else{
                $this->clean[$name] = $fields[$name];

            }
        }

    }
}