<?php

namespace core;

class Templater
{
    public function build($template, $params = [])
    {
        ob_start();
        extract($params);
        include_once $template;
        return ob_get_clean();
    }
}