<?php

namespace core;

use models\UserModel;


class User
{
    private $mUser;

    public function __construct(UserModel $mUser)
    {
        $this->mUser = $mUser;
    }

    public function signUp(array $fields)
    {
        if(!$this->checkPass($fields)){
            die('checkPassError');
        }

        $this->mUser->signUp($fields);


    }

    private function checkPass($fields)
    {

       return $fields['password'] === $fields['passwordReplay'] ? true : false;
    }
}