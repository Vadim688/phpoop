<?php

namespace controllers;

use core\DBConnector;
use core\Validator;
use models\PostModel;
use core\DBDriver;

class PostController extends BaseController
{
    public function indexAction()
    {
        $this->title .= ' главная';

        // записываем в переменную класс с статичным методом connect
        $db = new DBDriver(DBConnector::getConnect());

        // создаем объект от класса и передаем db
        $mPost = new PostModel($db, new Validator());

        $posts = $mPost->getAll();
        
        $this->content = $this->build(__DIR__ . '\..\views\posts.php', ['posts' => $posts]);
    }

    public function oneAction()
    {
        $id = $this->request->get('id');
        $db =  new DBDriver(DBConnector::getConnect());

        $mPost = new PostModel($db, new Validator());
        $post = $mPost->getById($id);
        $this->content = $this->build(__DIR__ . '\..\views\post.php', [
            'title' => $post['title'],
            'text' => $post['text'],
            'dt' => $post['dt'],
            'id' => $id
        ]);
    }

    public function deleteAction()
    {
        $id = $this->request->get('id');
        $db =  new DBDriver(DBConnector::getConnect());

        $mPost = new PostModel($db, new Validator());
        $mPost->delete($id);
        $this->redirect('/');
    }

    public function addAction()
    {
        $this->title .= ' добавить статью';
        if($this->request->isPost()){
            $db =  new DBDriver(DBConnector::getConnect());
            $mPost = new PostModel($db, new Validator());

            $mPost->add([
                'title' => $this->request->post('title'),
                'text' => $this->request->post('text'),
            ]);

            $this->redirect('/');

        }
        $this->content = $this->build(__DIR__ . '\..\views\add.php');
    }

    public function editAction()
    {
        $id = $this->request->get('id');
        $this->title .= ' редактировать статью';
        if($this->request->isPost()){
            $db =  new DBDriver(DBConnector::getConnect());
            $mPost = new PostModel($db, new Validator());

            $mPost->edit([

                'title' => $this->request->post('title'),
                'text' => $this->request->post('text'),
                'id' => $id
            ]);
            $this->redirect('/');
        }
        $db =  new DBDriver(DBConnector::getConnect());
        $mPost = new PostModel($db, new Validator());
        $post = $mPost->getById($id);
        $this->content = $this->build(__DIR__ . '\..\views\edit.php',[
            'title' => $post['title'],
            'text' => $post['text']
        ]);
    }

}
