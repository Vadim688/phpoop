<?php

namespace controllers;

use core\Request;

class BaseController
{
    protected $title;
    protected $content;
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->title = 'PHP2';
        $this->content = '';
    }

    public function render()
    {
        echo $this->build(__DIR__ . '/../views/main.php',
            [
                'title' => $this->title,
                'content' => $this->content
            ]);
    }

    public function build($template, $params = [])
    {
        ob_start();
        extract($params);
        include_once $template;
        return ob_get_clean();
    }

    public function redirect($uri)
    {
        return header(sprintf('Location: %s', $uri));
        die();
    }
}