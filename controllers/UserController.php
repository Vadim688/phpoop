<?php

namespace controllers;

use core\DBConnector;
use core\User;
use core\Validator;
use models\UserModel;
use core\DBDriver;

class UserController extends BaseController
{
    public function SignUpAction()
    {
        $this->title .= ' регистрация';

        if( $this->request->isPost()){
            $db =  new DBDriver(DBConnector::getConnect());
            $mUser = new UserModel($db, new Validator());

            $user = new User($mUser);
            $user->signUp($this->request->post());

            $this->redirect('/');
        }

        $this->content = $this->build(__DIR__ . '\..\views\signUp.php', []);
    }

    public function signInAction()
    {
        $errors = [];
        $this->title .= '  Авторизация';

        if( $this->request->isPost()){
            $db =  new DBDriver(DBConnector::getConnect());
            $mUser = new UserModel($db, new Validator());

            $user = new User($mUser);
            $user->signIn($this->request->post());

            $this->redirect('/');
        }

        $this->content = $this->build(__DIR__ . '\..\views\signIn.php', []);

    }



}
