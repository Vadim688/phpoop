<?php

use core\DBConnector; // использовать класс DB в пространстве имен core
use core\Templater;
use models\UserModel;
use models\PostModel;

// функция автолоада файлов
function __autoload($classname){
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
}

$uri = $_SERVER['REQUEST_URI'];

$uriParts = explode('/', $uri);
unset($uriParts[0]);
$uriParts = array_values($uriParts);

$controller = isset($uriParts[0]) && $uriParts[0] !== '' ? $uriParts[0] : 'post';

switch ($controller) {
    case 'post': 
        $controller = 'Post';
        break;
    case 'user': 
        $controller = 'User';
        break;
    default:
        header("HTTP/1.0 404 Not Found");
        die('error 404');
        break;
}


if(isset($uriParts[1]) && is_numeric($uriParts[1])){
    $id = $uriParts[1];
    $uriParts[1] = 'one';
}


$action = isset($uriParts[1]) && $uriParts[1] !== '' && is_string($uriParts[1]) ? $uriParts[1] : 'index';

$action = sprintf('%sAction', $action);

if(!$id){
    $id = isset($uriParts[2]) && is_numeric($uriParts[2]) ? $uriParts[2] : false;
}

if($id){
    $_GET['id'] = $id;
}
$request = new \core\Request($_GET, $_POST, $_SERVER, $_COOKIE, $_FILES, $_SESSION);
$controller = sprintf('controllers\%sController', $controller);
$controller = new $controller($request);
$controller->$action();
$controller->render();


