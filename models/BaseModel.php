<?php

namespace models;

use core\DBDriver;
use core\Validator;

abstract class BaseModel
{
    protected $db;
    protected $table;
    protected $validator;

    public function __construct(DBDriver $db, Validator $validator, $table) // \PDO - только экземпляр этого класса может быть передан
    {
        $this->db = $db;
        $this->table = $table;
        $this->validator = $validator;
    }
// получить все записи
    public function getAll()
    {
        $sql = "SELECT * FROM $this->table ORDER BY dt DESC";

        return $this->db->select($sql);
    }
// получить запись по id
    public function getById($id)
    {
        $sql = "SELECT * FROM $this->table WHERE id = :id";

        return $this->db->select($sql, ['id' => $id], 'one');
    }
// удалить запись по id
    public function delete($id)
    {
        $sql = "DELETE FROM $this->table WHERE id = :id";
        return $this->db->del($sql, ['id' => $id]);
    }

    public function add(array $params, $needValidation = true)
    {
        if($needValidation){

            $this->validator->execute($params);

            if(!$this->validator->success){
                die('xer');
                $this->validator->errors;
            }

            $params = $this->validator->clean;

        }

        return $this->db->insert($this->table, $params);
    }
///////////////////////////
    public function edit(array $params)
    {

        return $this->db->edits($this->table, $params);
    }

}