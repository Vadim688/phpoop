<?php

namespace models;

use core\DBDriver;
use core\Validator;

class UserModel extends BaseModel
{
    protected $schema = [
//        'id' => [
//            'primary' => true,
//        ],

        'login' => [
            'type' => 'string',
            'min_length' => 5,
            'max_length' => 50,
            'not_blank' => true,
            'require' => true
        ],

        'password' => [
            'type' => 'string',
            'min_length' => 8,
            'max_length' => 50,
            'not_blank' => true,
            'require' => true
        ],
    ];

    public function __construct(DBDriver $db, Validator $validator) // \PDO - только экземпляр этого класса может быть передан
    {
        parent::__construct($db, $validator, 'users');
        $this->validator->setRules($this->schema);
    }

    public function signUp(array $fields)
    {
        $this->validator->execute($fields);

        if(!$this->validator->success){
           die('wrong');
        }

        $this->add([
        'login' => $this->validator->clean['login'],
        'password' => $this->getHash($this->validator->clean['password'])
    ]);

    }

    public function getHash($password)
    {

        return hash('sha256', $password);
    }

}