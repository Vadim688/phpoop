<?php

namespace models;

use core\DBDriver;
use core\Validator;

class PostModel extends BaseModel
{
    protected $schema = [
//        'id' => [
//            'type' => 'integer',
//            'primary' => true,
//        ],

        'title' => [
            'type' => 'string',
            'min_length' => 5,
            'max_length' => 50,
            'not_blank' => true,
            'require' => true
        ],

        'text' => [
            'type' => 'string',
            'min_length' => 15,
            'max_length' => 500,
            'not_blank' => true,
            'require' => true
        ],
    ];
    public function __construct(DBDriver $db, Validator $validator) // \PDO - только экземпляр этого класса может быть передан
    {
        parent::__construct($db, $validator, 'posts');
        $this->validator->setRules($this->schema);
    }



}